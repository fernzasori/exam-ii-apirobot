*** Settings ***
Test Setup        Log To Console    <<<START>>>
Test Teardown     Log To Console    ------------------------------------------------------------------------------
Resource          ../KEYWORDS/Keywords.txt

*** Test Cases ***
WS2_ROBOT
    Comment    @{ListData}    [ER] Get Data Row For Excel    TESTDATA/DataTest.xlsx    DATA1    2
    Comment    [W] Open Browser    https://www.advantageonlineshopping.com/#/
    Comment    [W] Capture Page Screenshot
    Comment    [W] Click Element    //a[@id='hrefUserIcon']//*[local-name()='svg']
    Comment    [W] Capture Page Screenshot
    Comment    Sleep    3
    Comment    [W] Input Text    //input[@name='username']    kachaporn
    Comment    [W] Capture Page Screenshot
    Comment    Sleep    3
    Comment    [W] Input Text    //input[@name='password']    Passw0rd
    Comment    [W] Capture Page Screenshot
    Comment    Sleep    3
    Comment    [W] Click Element    //sec-sender[@class='roboto-medium ng-isolate-scope sec-sender']
    Comment    [W] Capture Page Screenshot
    Comment    [W] Click Element    //div[@id='miceImg']
    Comment    [W] Capture Page Screenshot
    Comment    [W] Click Element    //img[@id='32']
    Comment    [W] Capture Page Screenshot
    Comment    [W] Click Element    //div[@id='Description']//div[2]//span[2]
    Comment    [W] Capture Page Screenshot
    Comment    [W] Input Text    //input[@name='quantity']    10
    Comment    [W] Capture Page Screenshot
    Comment    [W] Click Element    //button[@name='save_to_cart']
    Comment    [W] Capture Page Screenshot
    Comment    [W] Click Element    //a[@id='shoppingCartLink']//*[local-name()='svg']
    Comment    [W] Capture Page Screenshot
    Comment    [W] Click Element    //button[@id='checkOutButton']
    Comment    [W] Capture Page Screenshot
    Comment    Sleep    3
    Comment    [W] Check Meaasge    ORDER PAYMENT
    Comment    [W] Capture Page Screenshot
    Comment    [W] Click Element    //button[@id='next_btn']
    Comment    [W] Capture Page Screenshot
    Comment    [W] Click Element    //button[@id='pay_now_btn_MasterCredit']
    Comment    [W] Capture Page Screenshot
    Comment    [W] Check Meaasge    Thank you for buying with Advantage
    Comment    [W] Capture Page Screenshot
    Comment    [W] Click Element    //span[@class='hi-user containMiniTitle ng-binding']
    Comment    [W] Capture Page Screenshot
    Comment    [W] Click Element    //label[@class='option roboto-medium ng-scope'][contains(text(),'Sign out')]
    Comment    [W] Capture Page Screenshot
    Comment    [W] Close Browser

TC02_TESTAPI
    Comment    @{ListData}    [ER] Get Data Row For Excel    TESTDATA/DataTest.xlsx    DATA3    2
    Comment    Log    @{ListData}[0]
    Comment    Log    @{ListData}[1]
    Comment    Log    @{ListData}[2]
    Comment    [AI]Get Request Data    @{ListData}[0]    @{ListData}[1]    @{ListData}[2]

TC03_TESTPOST
    Comment    @{ListData}    [ER] Get Data Row For Excel    TESTDATA/DataTest.xlsx    DATA4    2
    Comment    Log    @{ListData}[0]
    Comment    Log    @{ListData}[1]
    Comment    [AI]Post Request Data    @{ListData}[0]    @{ListData}[1]

TC00_GetJobJenkins
    Comment    @{ListData}    [ER] Get Data Row For Excel    TESTDATA/DataTest.xlsx    Jenkins    2
    Comment    @{Auth}=    Create List    @{ListData}[0]    @{ListData}[1]
    Comment    [AI]Get Job Jenkins    @{ListData}[2]    @{ListData}[3]    @{ListData}[4]    @{Auth}

TC001_DogRandomFernGet
    @{ListData}    [ER] Get Data Row For Excel    TESTDATA/DataTest.xlsx    DATA4    2
    Log    @{ListData}[0]
    Log    @{ListData}[1]
    [AI]Get Request Data    @{ListData}[0]    @{ListData}[1]

TC002_EmployeeFernPost
    @{ListData}    [ER] Get Data Row For Excel    TESTDATA/DataTest.xlsx    DATA3    2
    Log    @{ListData}[0]
    Log    @{ListData}[1]
    Log    @{ListData}[2]
    [AI]Post Request Data    @{ListData}[0]    @{ListData}[1]    @{ListData}[2]
